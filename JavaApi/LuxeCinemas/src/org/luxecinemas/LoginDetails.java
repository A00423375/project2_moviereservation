package org.luxecinemas;

public class LoginDetails {
	
	
		private int id;
		private int customerId;
		private String loginId;
		private String password;
		
		
		public LoginDetails( ) {

		}	
		
		public LoginDetails( int ID,int customerId,String logid,String passwor) {
			
			this.id=ID;
			this.customerId=customerId;
			this.loginId = logid;
			this.password = passwor;
			
			
		}
		
		public int getid() {
			return id;
		}
		
		public void setid(int ID) {
			this.id = ID;
		}
		
		
		public int getcustomerId() {
			return customerId;
		}
		
		public void setcustomerId(int CustId) {
			this.customerId = CustId;
		}
		
			
		public String getloginId() {
			return loginId;
		}
		
		public void setloginId(String logid) {
			this.loginId = logid;
		}
		
		public String getpassword() {
			return password;
		}
		
		public void setpassword(String passwor) {
			this.password = passwor;
		}
		
		
		
		
}
