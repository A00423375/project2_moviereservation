package org.luxecinemas;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.util.List;

import javax.ws.rs.DELETE;

import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

@Path("/Customers")
public class CustomerController {

    // URI:
    // /contextPath/servletPath/Customers
    @GET
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Customer> getEmployees_JSON() {
    	luxecinemas dao = new JDBCCustomerDAO();
        List<Customer> listOfCustomers = dao.getAllCustomers();
        return listOfCustomers;
    }
 
    
    // URI:
    // /contextPath/servletPath/Customers
    @POST
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public String addCustomer(Customer cust) {
    	luxecinemas dao = new JDBCCustomerDAO();
    	return dao.addCustomer(cust);
    	
    }
 
   
	
	
}
