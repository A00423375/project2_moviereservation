package org.luxecinemas;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;


	
	@Path("/LoginDetails")
	public class LoginDetailsController {

		// URI:
	    // /contextPath/servletPath/LoginDetails
		@GET
		@Path("/login")
	    @Produces(MediaType.TEXT_PLAIN)
		public String validateLoginDetails_JSON(@DefaultValue("u1") @QueryParam("loginId") String loginId ,
				 @DefaultValue("u1") @QueryParam("pwd") String pwd) {
			LoginDetailsDAO dao = new JDBCLoginDetailsDAO();
	        return dao.validateLoginDetails(loginId,pwd);	
		}
	    
	 // URI:
	    // /contextPath/servletPath/LoginDetails
	    @POST
	    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })	   
	    public String addLoginDetails(LoginDetails logdetail) {
	    	LoginDetailsDAO dao = new JDBCLoginDetailsDAO();
	        return dao.addLoginDetails(logdetail);
	    }
}
