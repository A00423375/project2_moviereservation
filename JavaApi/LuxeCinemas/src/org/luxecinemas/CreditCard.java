package org.luxecinemas;

public class CreditCard {

	private int id;
	private int customerId;
	private String nameOnCard;
	private String cardType;
	private String cardNumber;
	private String securityPin;
	private String expiryDate;
	
	
	public CreditCard( ) {

	}	
	
	public CreditCard( int id, int CustomerId, String NameOnCard, String CardType, String CardNumber, String SecurityPin, String ExpiryDate) {
		this.id=id;
		this.customerId = CustomerId;
		this.nameOnCard = NameOnCard;
		this.cardType = CardType;
		this.cardNumber = CardNumber;
		this.securityPin = SecurityPin;
		this.expiryDate = ExpiryDate;
		
		
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int CustomerId) {
		this.customerId = CustomerId;
	}
	
	public String getNameOnCard() {
		return nameOnCard;
	}
	public void setNameOnCard(String NameOnCard) {
		this.nameOnCard = NameOnCard;
	}
	
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String CardType) {
		this.cardType = CardType;
	}
	
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String CardNumber) {
		this.cardNumber = CardNumber;
	}
	
	public String getSecurityPin() {
		return securityPin;
	}
	public void setSecurityPin(String SecurityPin) {
		this.securityPin = SecurityPin;
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String ExpiryDate) {
		this.expiryDate = ExpiryDate;
	}
	
}

