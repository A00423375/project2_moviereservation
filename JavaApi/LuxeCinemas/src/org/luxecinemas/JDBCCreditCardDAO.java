package org.luxecinemas;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;


public class JDBCCreditCardDAO extends JDBCPooledConnection implements CreditCardDAO {
    
	@Override
	public String addCreditCard(CreditCard creditcard) {
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("INSERT INTO luxecinemas.creditcard (CustomerId ,NameOnCard ,CardType ,CardNumber ,SecurityPin ,ExpiryDate) VALUES (? ,? ,? ,? ,? ,?)");
            preparedStatement.setInt(1,  creditcard.getCustomerId());            
            preparedStatement.setString(2,  creditcard.getNameOnCard());
            preparedStatement.setString(3,  creditcard.getCardType());
            preparedStatement.setString(4,  creditcard.getCardNumber());            
            preparedStatement.setString(5,  creditcard.getSecurityPin());
            preparedStatement.setString(6,  creditcard.getExpiryDate());            
            preparedStatement.executeUpdate();
            preparedStatement.close();
            closeConnection();
            return "Success";
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            closeConnection();
           // return e.toString();
            return "Invalid";
        }
    }
 

	@Override   
    public List<CreditCard> getAllCreditCards() {
        List<CreditCard> persons = new LinkedList<CreditCard>();
         try {
                Statement statement = getConnection().createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * from creditcard");
                 
                CreditCard creditcar = null;
                while(resultSet.next()){
                	creditcar = new CreditCard();
                	creditcar.setId(Integer.parseInt(resultSet.getString("ID")));
                	creditcar.setCustomerId(Integer.parseInt(resultSet.getString("CustomerId")));
                	creditcar.setNameOnCard(resultSet.getString("NameOnCard"));
                	creditcar.setCardType(resultSet.getString("CardType"));
                	creditcar.setCardNumber(resultSet.getString("CardNumber"));                	
                	creditcar.setSecurityPin(resultSet.getString("SecurityPin"));
                	creditcar.setExpiryDate(resultSet.getString("ExpiryDate"));
                    persons.add(creditcar);
                }
                resultSet.close();
                statement.close();
                closeConnection();
            } catch (SQLException e) {
            	closeConnection();
                e.printStackTrace();
            }
            System.out.println(persons);
            return persons;
    }
}
