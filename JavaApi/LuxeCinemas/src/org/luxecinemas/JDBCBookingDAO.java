package org.luxecinemas;

//import java.sql.Connection;
//import java.sql.Connection;
//import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class JDBCBookingDAO extends JDBCPooledConnection implements BookingDAO{


    @Override
    public String addBooking(Bookings booking) {
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("INSERT INTO luxecinemas.bookings (CustomerId ,Movie ,Tickets ,Date ,Time) VALUES (? ,? ,? ,? ,?)");
            preparedStatement.setInt(1,  booking.getCustomerId());
            preparedStatement.setString(2,  booking.getMovie());
            preparedStatement.setString(3,  booking.getTickets());
            preparedStatement.setString(4,  booking.getdate());
            preparedStatement.setString(5,  booking.getTime());

            preparedStatement.executeUpdate();
            return "Success";
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            //return "Invalid";
            return e.toString();
        }
    }


    @Override
   public List<Bookings> getAllBookings() {
        List<Bookings> bookings = new LinkedList<Bookings>();
         try {
                Statement statement = getConnection().createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM bookings");

                Bookings booking = null;
                while(resultSet.next()){
                    booking = new Bookings();
                    booking.setId(Integer.parseInt(resultSet.getString("ID")));
                    booking.setCustomerId(Integer.parseInt(resultSet.getString("CustomerId")));
                    booking.setMovie(resultSet.getString("Movie"));
                    booking.setTickets(resultSet.getString("Tickets"));
                    booking.setDate(resultSet.getString("Date"));
                    booking.setTime(resultSet.getString("Time"));
                    bookings.add(booking);
                }
                resultSet.close();
                statement.close();
                closeConnection();

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return bookings;
    }
}
