package org.luxecinemas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

import javax.sql.DataSource;
import javax.enterprise.context.spi.Context;
import javax.enterprise.context.spi.Contextual;
import javax.naming.InitialContext;
import javax.naming.NamingException;
 

public class JDBCPooledConnection {
 
    Connection conn = null;   
    
    public final Connection getConnection(){
		Logger logger = Logger.getAnonymousLogger();
        logger.setLevel(Level.ALL);
        logger.addHandler(new StreamHandler(System.out, new SimpleFormatter()));
    	Connection conn = null;
    	try {
    		logger.log(Level.INFO, "Getting connection from connection pool.");
	        DataSource ds = (DataSource)(new InitialContext().lookup("java:/comp/env/jdbc/luxecinemas"));
    		conn = ds.getConnection();
    		logger.log(Level.INFO, "Got connection: " + conn);
    	}
    	catch(NamingException | SQLException ex) {
    		logger.log(Level.SEVERE, "Failed to get connection.");
    		ex.printStackTrace();
    	}
    	this.conn = conn;
        return conn;
    }

    public void closeConnection(){
		Logger logger = Logger.getAnonymousLogger();
        logger.setLevel(Level.ALL);
        logger.addHandler(new StreamHandler(System.out, new SimpleFormatter()));
        try {
              if (conn != null) {
            	  conn.close();
              }
            } catch (Exception e) { 
            	logger.log(Level.SEVERE, "Failed to close connection.");
            	logger.log(Level.SEVERE, "Connection:" + conn);
            }
    } 
}