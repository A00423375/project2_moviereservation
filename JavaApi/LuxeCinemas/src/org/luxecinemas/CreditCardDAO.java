package org.luxecinemas;

import java.util.List;

public interface CreditCardDAO {
	String addCreditCard(CreditCard creditcard);

	List<CreditCard> getAllCreditCards();

}
