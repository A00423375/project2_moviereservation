package org.luxecinemas;


public class Customer {

	private int id;
	private String firstName;
	private String lastName;
	private String city;
	private String province;
	private String country;
	private String postal;
	private String phone;
	private String email;
	private String address;
	private String loginId;
	private String password;
	
	
	public Customer( ) {

	}	
	
	public Customer( int id, String fName, String lName, String city, String province, String country, String postal, String phone, String email, String address, String loginId, String password) {
		this.id=id;
		this.firstName = fName;
		this.lastName =lName;
		this.city = city;
		this.province = province;
		this.country = country;
		this.postal = postal;
		this.phone = phone;
		this.email = email;
		this.address = address;
		this.loginId = loginId;
		this.password = password;
		
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getPostal() {
		return postal;
	}
	public void setPostal(String postal) {
		this.postal = postal;
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getloginId() {
		return loginId;
	}
	public void setloginId(String loginId) {
		this.loginId = loginId;
	}
	
	public String getpassword() {
		return password;
	}
	public void setpassword(String password) {
		this.password = password;
	}
	
	
	
}
