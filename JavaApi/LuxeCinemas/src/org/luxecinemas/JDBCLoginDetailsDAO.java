package org.luxecinemas;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;


	public class JDBCLoginDetailsDAO extends JDBCPooledConnection implements LoginDetailsDAO {
		
		@Override
		public String addLoginDetails(LoginDetails logdetail) {
	        try {
	            PreparedStatement preparedStatement = getConnection().prepareStatement("INSERT INTO luxecinemas.login (CustomerId,loginId,password) VALUES (? ,? ,?)");
	            preparedStatement.setInt(1,  logdetail.getcustomerId());
	            preparedStatement.setString(2,  logdetail.getloginId());
	            preparedStatement.setString(3,  logdetail.getpassword());
	            preparedStatement.executeUpdate();	            
	            preparedStatement.close();	    
	            closeConnection();
	            return validateLoginDetails(logdetail.getloginId(),logdetail.getpassword());
	        } catch (SQLException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	           // return e.toString();
	            return "Invalid";
	        }
	    }
	 

		@Override   
	    public String validateLoginDetails(String logid,String passwor) {
			try {
				Statement statement = getConnection().createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * from Login where (loginId= '"+logid+"' and password='"+passwor+"')");
				if(resultSet.next()) {
					return resultSet.getString("CustomerId");
				}
				else {
					return "Invalid";
				}
				closeConnection();	
			} catch (SQLException e) {
				e.printStackTrace();
				return "Invalid";
			}
	    }
}
