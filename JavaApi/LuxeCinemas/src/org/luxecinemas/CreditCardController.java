package org.luxecinemas;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;



@Path("/CreditCards")
public class CreditCardController {

	// URI:
    // /contextPath/servletPath/Creditcards
    @GET
    @Produces({ MediaType.APPLICATION_JSON})
    public List<CreditCard> getCreditCards_JSON() {
    	CreditCardDAO dao = new JDBCCreditCardDAO();
        List<CreditCard> listOfCreditCards = dao.getAllCreditCards();
        return listOfCreditCards;
    }
    
     
 // URI:
    // /contextPath/servletPath/CreditCards
    @POST
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    
    public String addCreditCard(CreditCard creditcard) {
    	
    	CreditCardDAO dao = new JDBCCreditCardDAO();
        return dao.addCreditCard(creditcard);
    }
    
       
}
