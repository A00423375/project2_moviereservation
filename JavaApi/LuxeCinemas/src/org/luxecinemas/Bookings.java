package org.luxecinemas;

public class Bookings {
	private int id;
	private int customerId;
	private String movie;
	private String tickets;
	private String date;
	private String time;
	
	
	
	public Bookings( ) {

	}	
	
	public Bookings( int id, int CustomerId, String movie, String tickets, String date, String time) {
		this.id=id;
		this.customerId = CustomerId;
		this.movie = movie;
		this.tickets = tickets;
		this.date = date;
		this.time = time;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int CustomerId) {
		this.customerId = CustomerId;
	}
	
	public String getMovie() {
		return movie;
	}
	public void setMovie(String movie) {
		this.movie = movie;
	}
	
	public String getTickets() {
		return tickets;
	}
	public void setTickets(String tickets) {
		this.tickets = tickets;
	}
	
	public String getdate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	
}