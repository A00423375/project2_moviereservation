package org.luxecinemas;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.util.List;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;



@Path("/Bookings")
public class BookingsController {

	// URI:
    // /contextPath/servletPath/Bookings
    @GET
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Bookings> getBookings_JSON() {
    	BookingDAO dao = new JDBCBookingDAO();
        List<Bookings> listOfBookings = dao.getAllBookings();
        return listOfBookings;
    }
    
     
 // URI:
    // /contextPath/servletPath/Bookings
    @POST
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    
    public String addBooking(Bookings booking) {
    	
    	BookingDAO dao = new JDBCBookingDAO();
        return dao.addBooking(booking);
    }
    
       
}
