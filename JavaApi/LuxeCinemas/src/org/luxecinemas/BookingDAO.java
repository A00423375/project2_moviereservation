package org.luxecinemas;

import java.util.List;

public interface BookingDAO {
	
	String addBooking(Bookings booking);

	List<Bookings> getAllBookings();

}
