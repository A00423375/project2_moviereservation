package org.luxecinemas;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
 

public class JDBCCustomerDAO extends JDBCPooledConnection implements luxecinemas{

	@Override
	public String addCustomer(Customer customer) {		
        try {
            PreparedStatement preparedStatement = getConnection().prepareStatement("INSERT INTO luxecinemas.customer (firstname ,lastName ,city ,province ,country ,postal ,phone ,email ,address) VALUES (? ,? ,? ,? ,? ,? ,? ,? ,?)",Statement.RETURN_GENERATED_KEYS);
        	
        	
        	preparedStatement.setString(1,  customer.getFirstName());
            preparedStatement.setString(2,  customer.getLastName());
            preparedStatement.setString(3,  customer.getCity());
            preparedStatement.setString(4,  customer.getProvince());
            preparedStatement.setString(5,  customer.getCountry());
            preparedStatement.setString(6,  customer.getPostal());
            preparedStatement.setString(7,  customer.getPhone());
            preparedStatement.setString(8,  customer.getEmail());
            preparedStatement.setString(9,  customer.getAddress());
            preparedStatement.executeUpdate();
            ResultSet rs=preparedStatement.getGeneratedKeys();
            int result;
            if(rs.next()){
            	result = rs.getInt(1);
            	//return Integer.toString(result);
            	LoginDetails loginObj = new LoginDetails();
            	loginObj.setcustomerId(result);
            	loginObj.setloginId(customer.getloginId());
            	loginObj.setpassword(customer.getpassword());
            	JDBCLoginDetailsDAO login = new JDBCLoginDetailsDAO();
            	closeConnection();
            	return login.addLoginDetails(loginObj);
            	
            }
            else
            {        
            	closeConnection();
            	return "Invalid";
            }
            
            
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            closeConnection();
            return "Invalid";
            //return e.toString();
        }
       
         
    }
 

	@Override   
   public List<Customer> getAllCustomers() {
        List<Customer> persons = new LinkedList<Customer>();
         try {
                Statement statement = getConnection().createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM customer");
                 
                Customer customer = null;
                while(resultSet.next()){
                	customer = new Customer();
                	customer.setId(Integer.parseInt(resultSet.getString("ID")));
                	customer.setFirstName(resultSet.getString("firstName"));
                	customer.setLastName(resultSet.getString("lastName"));
                	customer.setCity(resultSet.getString("city"));
                	customer.setProvince(resultSet.getString("province"));
                	customer.setCountry(resultSet.getString("country"));
                	customer.setPostal(resultSet.getString("postal"));
                	customer.setPhone(resultSet.getString("phone"));
                	customer.setEmail(resultSet.getString("email"));
                	customer.setAddress(resultSet.getString("address"));                     
                    persons.add(customer);
                }
                resultSet.close();
                statement.close();
                closeConnection();
                 
            } catch (SQLException e) {
                e.printStackTrace();
                closeConnection();
            }
            System.out.println(persons);
            return persons;
    }
}