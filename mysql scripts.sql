 create table Customer(
   ID INT NOT NULL AUTO_INCREMENT,
   firstName VARCHAR(100) NOT NULL,
   lastName VARCHAR(100) NOT NULL,
   city VARCHAR(100) NOT NULL,
   province VARCHAR(100) NOT NULL,
   country VARCHAR(100) NOT NULL,
   postal VARCHAR(100) NOT NULL,
   phone VARCHAR(100) NOT NULL,
   email VARCHAR(100) NOT NULL,
   address VARCHAR(100) NOT NULL,   
   PRIMARY KEY ( ID )
);




 create table Bookings(
   ID INT NOT NULL AUTO_INCREMENT,
   CustomerId INT NOT NULL,
   Movie VARCHAR(100) NOT NULL,
   Tickets VARCHAR(100) NOT NULL,
   province VARCHAR(100) NOT NULL,
   Date VARCHAR(100) NOT NULL,
   Time VARCHAR(100) NOT NULL,   
   PRIMARY KEY ( ID )
);

ALTER TABLE Bookings ADD CONSTRAINT fk_Cust_id FOREIGN KEY (CustomerId) REFERENCES Customer(ID);


 create table CreditCard(
   ID INT NOT NULL AUTO_INCREMENT,
   CustomerId INT NOT NULL,
   NameOnCard VARCHAR(100) NOT NULL,
   CardType VARCHAR(100) NOT NULL,
   CardNumber VARCHAR(100) NOT NULL,
   SecurityPin VARCHAR(10) NOT NULL,
   ExpiryDate VARCHAR(10) NOT NULL,   
   PRIMARY KEY ( ID )
);



ALTER TABLE CreditCard ADD CONSTRAINT fk_CreditCust_id FOREIGN KEY (CustomerId) REFERENCES Customer(ID);


create table Login(
   ID INT NOT NULL AUTO_INCREMENT,
   CustomerId INT NOT NULL,
   loginId VARCHAR(100) NOT NULL,
   password VARCHAR(100) NOT NULL,   
   PRIMARY KEY ( ID )
);




ALTER TABLE Login ADD CONSTRAINT fk_LoginCust_id FOREIGN KEY (CustomerId) REFERENCES Customer(ID);