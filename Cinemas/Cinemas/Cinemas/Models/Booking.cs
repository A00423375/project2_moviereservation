//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------



namespace Cinemas.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Booking
    {

        public int ID { get; set; }

        public int CustomerId { get; set; }

        [Display(Name = "Movie")]
        [Required(ErrorMessage = "Movie is Required")]
        public string Movie { get; set; }

        [Display(Name = "Tickets")]
        [Required(ErrorMessage = "Ticket is Required")]
        public string Tickets { get; set; }

        [Display(Name = "Date")]
        [Required(ErrorMessage = "Date is Required")]
        [DateValidation]
        public string Date { get; set; }

        [Display(Name = "Time")]
        [Required(ErrorMessage = "Time Required")]
        public string Time { get; set; }       

        public virtual Customer Customer { get; set; }
    }
}

