﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Reflection;

namespace Cinemas
{
    public class CardValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value,
                            ValidationContext validationContext)
        {

            bool flag = false;
            object instance = validationContext.ObjectInstance;
            Type type = instance.GetType();
            PropertyInfo property = type.GetProperty("CardType");
            object propertyValue = property.GetValue(instance);

            if (propertyValue == null)
            {
                flag = false;
            }
            else
            {
                switch (propertyValue.ToString())
                {
                    case "Master":
                        Regex regex = new Regex(@"^5[1-5]\d+$");
                        if (regex.IsMatch(value.ToString()))
                        {
                            if (value.ToString().Length == 16)
                            {
                                flag = true;
                            }
                        }                       
                        break;
                    case "Visa":
                        Regex regex1 = new Regex(@"^4\d+$");
                        if (regex1.IsMatch(value.ToString()))                           
                        {
                            if (value.ToString().Length == 16)
                            {
                                flag = true;
                            }
                        }
                        break;
                    case "Amex":
                        Regex regex2 = new Regex(@"^34|37\d+$");
                        if (regex2.IsMatch(value.ToString()))                           
                        {
                            if (value.ToString().Length == 15)
                            {
                                flag = true;
                            }
                        }
                        break;
                    default:
                        flag = false;
                        break;
                }
            }

            if(flag)
            {
                return null;
            }
            else
            {
                ValidationResult result = new ValidationResult
                    ("Invalid Card number for the selected Card Type!");
                return result;
            }
        }

        
    
}
}




        



