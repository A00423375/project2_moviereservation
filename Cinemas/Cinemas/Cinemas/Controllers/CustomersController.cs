﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cinemas.Models;
using Newtonsoft.Json;

namespace Cinemas.Controllers
{
    public class CustomersController : Controller
    {
        private LuxeCinemasEntities db = new LuxeCinemasEntities();

        string Baseurl = @"http://localhost:8080/LuxeCinemas/api/";
        public async Task<ActionResult> Index()
        {
            List<Customer> custInfo = new List<Customer>();

            using (var client = new HttpClient())
            {
                try
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(Baseurl);

                    client.DefaultRequestHeaders.Clear();
                    //Define request data format  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    HttpResponseMessage Res = await client.GetAsync("Customers");

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (Res.IsSuccessStatusCode)
                    {
                        //Storing the response details recieved from web api   
                        var CustResponse = Res.Content.ReadAsStringAsync().Result;

                        //Deserializing the response recieved from web api and storing into the Employee list  
                        custInfo = JsonConvert.DeserializeObject<List<Customer>>(CustResponse);

                    }
                    //returning the employee list to view  
                    return View(custInfo);
                    }
                catch(Exception ex)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }
            }
        }
        // GET: Customers
        //public ActionResult Index()
        //{
        //    try
        //    {
        //        return View(db.Customers.ToList());
        //    }
        //    catch (Exception ex)
        //    {
        //        return View("~/Views/Shared/Error.cshtml");
        //    }
        //}

        // GET: Customers/Details/5
        public ActionResult Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Customer customer = db.Customers.Find(id);
                if (customer == null)
                {
                    return HttpNotFound();
                }
                return View(customer);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // GET: Customers/Create
        public ActionResult Create()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // POST: Customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ID,firstName,lastName,city,province,country,postal,phone,email,address")] Customer customer)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            db.Customers.Add(customer);
        //            db.SaveChanges();

        //            Login loginObj = new Login();
        //            loginObj.CustomerId = customer.ID;
        //            Session["custId"] = Convert.ToString(customer.ID);
        //            loginObj.loginId = Request["loginName"].ToString();
        //            loginObj.password = Request["password"].ToString();
        //            db.Logins.Add(loginObj);
        //            db.SaveChanges();
        //            //return RedirectToAction("Index");
        //            return RedirectToAction("Create", "CreditCards");
        //        }

        //        return View(customer);
        //    }
        //    catch (Exception ex)
        //    {
        //        return View("~/Views/Shared/Error.cshtml");
        //    }

        //}

        [HttpPost]
        public async Task<ActionResult> Create(Customer customer)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (var client = new HttpClient())
                    {
                        string addcustomerMethod = "Customers";
                        HttpResponseMessage responseMessage = await client.PostAsJsonAsync(Baseurl + addcustomerMethod, customer);
                        if (responseMessage.IsSuccessStatusCode)
                        {

                            var CustResponse = responseMessage.Content.ReadAsStringAsync().Result;

                            //Deserializing the response recieved from web api and storing into the Employee list 
                            if (!string.Equals(JsonConvert.DeserializeObject<string>(CustResponse), "Invalid"))
                            {
                                Session["custId"] = JsonConvert.DeserializeObject<string>(CustResponse);
                                return RedirectToAction("Create", "CreditCards");
                            }
                            else
                            {
                                return View("~/Views/Shared/Error.cshtml");
                            }
                        }
                        else
                        {
                            return View("~/Views/Shared/Error.cshtml");
                        }

                    }
                }
                return View(customer);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
    
}
        // GET: Customers/Edit/5
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Customer customer = db.Customers.Find(id);
                if (customer == null)
                {
                    return HttpNotFound();
                }
                return View(customer);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,firstName,lastName,city,province,country,postal,phone,email,address")] Customer customer)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(customer).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(customer);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // GET: Customers/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Customer customer = db.Customers.Find(id);
                if (customer == null)
                {
                    return HttpNotFound();
                }
                return View(customer);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Customer customer = db.Customers.Find(id);
                db.Customers.Remove(customer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
