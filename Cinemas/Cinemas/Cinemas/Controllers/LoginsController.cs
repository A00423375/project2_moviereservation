﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cinemas.Models;
using Newtonsoft.Json;

namespace Cinemas.Controllers
{
    public class LoginsController : Controller
    {
        string Baseurl = @"http://localhost:8080/LuxeCinemas/api/";
        private LuxeCinemasEntities db = new LuxeCinemasEntities();

        // GET: Logins
        public ActionResult Index()
        {
            try
            {
                //var logins = db.Logins.Include(l => l.Customer);
                //return View(logins.ToList());
                return View();
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }


        // GET: Logins/Details/5
        public ActionResult Details()
        {
            try
            {
                var logins = db.Logins.Include(l => l.Customer);
                return View(logins.ToList());
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // GET: Logins/Create
        //public ActionResult Create()
        //{
        //    try
        //    {
        //        ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName");
        //        return View();
        //    }
        //    catch (Exception ex)
        //    {
        //        return View("~/Views/Shared/Error.cshtml");
        //    }
        //}


        // POST: Logins/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ID,CustomerId,loginId,password,IsAdmin")] Login login)
        //{
        //    try
        //    {


        //        if (ModelState.IsValid)
        //        {
        //            db.Logins.Add(login);
        //            db.SaveChanges();
        //            return RedirectToAction("Index");
        //        }

        //        ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName", login.CustomerId);
        //        return View(login);
        //    }
        //    catch (Exception ex)
        //    {
        //        return View("~/Views/Shared/Error.cshtml");
        //    }
        //}

        //GET: Logins/Create
        public async Task<ActionResult> Validate(Login login)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(Baseurl);

                    client.DefaultRequestHeaders.Clear();
                    //Define request data format  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));

                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    HttpResponseMessage Res = await client.GetAsync(string.Format("LoginDetails/login?loginId={0}&pwd={1}", login.loginId, login.password));

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (Res.IsSuccessStatusCode)
                    {

                        var CustResponse = Res.Content.ReadAsStringAsync().Result;

                        //Deserializing the response recieved from web api and storing into the Employee list 
                        if (!string.Equals(JsonConvert.DeserializeObject<string>(CustResponse), "Invalid"))
                        {
                            Session["custId"] = JsonConvert.DeserializeObject<string>(CustResponse);
                            return RedirectToAction("Create", "Bookings");
                        }
                        else
                        {
                            return View("~/Views/Shared/LoginFailed.cshtml");
                        }

                    }
                    else
                    {
                        return View("~/Views/Shared/Error.cshtml");
                    }
                    
                    //returning the employee list to view  

                }
                catch (Exception ex)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }
            }
        }

            


        //[HttpPost]
        //public ActionResult Validate(Login login)
        //{
        //    try
        //    {
        //        //var result = db1.Logins.Where(cls => cls.loginId.Equals(login.ID) && cls.password.Equals(login.password)).Any();
        //        string loginName = login.loginId;
        //        string pwd = login.password;
        //        var record = db.Logins.Where(u => u.loginId == loginName && u.password == pwd).FirstOrDefault();

        //        if (record != null && !string.IsNullOrEmpty(Convert.ToString(record.CustomerId)))
        //        {
        //            Session["CustId"] = Convert.ToString(record.CustomerId);
        //            return RedirectToAction("Create", "Bookings");

        //        }
        //        else
        //        {
        //            return View("~/Views/Shared/LoginFailed.cshtml");
        //        }
        //    }


        //    catch (Exception ex)
        //    {
        //        return View("~/Views/Shared/Error.cshtml");
        //    }


        //}


        // GET: Logins/Edit/5
        public ActionResult Edit(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Login login = db.Logins.Find(id);
                if (login == null)
                {
                    return HttpNotFound();
                }
                ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName", login.CustomerId);
                return View(login);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // POST: Logins/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CustomerId,loginId,password,IsAdmin")] Login login)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(login).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName", login.CustomerId);
                return View(login);
            }

            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // GET: Logins/Delete/5
        public ActionResult Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Login login = db.Logins.Find(id);
                if (login == null)
                {
                    return HttpNotFound();
                }
                return View(login);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        // POST: Logins/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Login login = db.Logins.Find(id);
                db.Logins.Remove(login);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }

        protected override void Dispose(bool disposing)
        {

            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);


        }
    }
}
