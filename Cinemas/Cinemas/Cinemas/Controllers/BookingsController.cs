﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cinemas.Models;
using Newtonsoft.Json;

namespace Cinemas.Controllers
{
    public class BookingsController : Controller
    {
        private LuxeCinemasEntities db = new LuxeCinemasEntities();

        // GET: Bookings
        //public ActionResult Index()
        //{
        //    var bookings = db.Bookings.Include(b => b.Customer);
        //    return View(bookings.ToList());
        //}

        // GET: Bookings/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Booking booking = db.Bookings.Find(id);
        //    if (booking == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(booking);
        //}

        // GET: Bookings/Details/5
        public ActionResult Details()
        {
            try
            {

                return View();
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
        }
        string Baseurl = @"http://localhost:8080/LuxeCinemas/api/";
        public async Task<ActionResult> Index()
        {
            List<Booking> bookingInfo = new List<Booking>();

            using (var client = new HttpClient())
            {
                try
                {
                    //Passing service base url  
                    client.BaseAddress = new Uri(Baseurl);

                    client.DefaultRequestHeaders.Clear();
                    //Define request data format  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                    HttpResponseMessage Res = await client.GetAsync("Bookings");

                    //Checking the response is successful or not which is sent using HttpClient  
                    if (Res.IsSuccessStatusCode)
                    {
                        //Storing the response details recieved from web api   
                        var bookingResponse = Res.Content.ReadAsStringAsync().Result;

                        if (!string.IsNullOrEmpty(bookingResponse))
                        {
                            bookingInfo = JsonConvert.DeserializeObject<List<Booking>>(bookingResponse);                            
                            return View(bookingInfo);
                        }
                        else
                        {
                            return View("~/Views/Shared/Error.cshtml");
                        }
                    }
                    else
                    {
                        return View("~/Views/Shared/Error.cshtml");
                    }
                    //returning the employee list to view  

                }
                catch (Exception ex)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }
            }
        }
        // GET: Bookings/Create
        public ActionResult Create()
        {
            ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName");
            return View();
        }

        // POST: Bookings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "ID,CustomerId,Movie,Tickets,Date,Time")] Booking booking)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            Booking book = new Booking();
        //            book.CustomerId = Convert.ToInt32(Session["CustId"]);                    
        //            book.Movie = booking.Movie;
        //            book.Tickets = booking.Tickets;
        //            book.Date = Convert.ToString(booking.Date);
        //            book.Time = booking.Time;
        //            db.Bookings.Add(book);
        //            db.SaveChanges();
        //            return RedirectToAction("Details", "Bookings");
        //        }


        //        ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName", booking.CustomerId);
        //        return View(booking);
        //    }
        //    catch(Exception ex)
        //    {
        //        return View("~/Views/Shared/Error.cshtml");
        //    }
        //}


        [HttpPost]
        public async Task<ActionResult> Create(Booking bookingObj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (var client = new HttpClient())
                    {

                        Booking book = new Booking();
                        book.Movie = bookingObj.Movie;
                        book.Tickets = bookingObj.Tickets;
                        book.CustomerId = Convert.ToInt32(Session["CustId"]);
                        book.Date = bookingObj.Date;
                        book.Time = bookingObj.Time;

                        string addbookMethod = "Bookings";
                        HttpResponseMessage responseMessage = await client.PostAsJsonAsync(Baseurl + addbookMethod, book);
                        if (responseMessage.IsSuccessStatusCode)
                        {

                            var bookResponse = responseMessage.Content.ReadAsStringAsync().Result;

                            //Deserializing the response recieved from web api and storing into the Employee list 
                            if (string.Equals(bookResponse, "Success"))
                            {
                                return RedirectToAction("Details", "Bookings");
                            }
                            else
                            {
                                return View("~/Views/Shared/Error.cshtml");
                            }
                        }
                        else
                        {
                            return View("~/Views/Shared/Error.cshtml");
                        }

                    }
                }
                return View(bookingObj);
            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");
            }

        }
        // GET: Bookings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Booking booking = db.Bookings.Find(id);
            if (booking == null)
            {
                return HttpNotFound();
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName", booking.CustomerId);
            return View(booking);
        }

        // POST: Bookings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CustomerId,Movie,Tickets,Date,Time")] Booking booking)
        {
            if (ModelState.IsValid)
            {
                db.Entry(booking).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CustomerId = new SelectList(db.Customers, "ID", "firstName", booking.CustomerId);
            return View(booking);
        }

        // GET: Bookings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Booking booking = db.Bookings.Find(id);
            if (booking == null)
            {
                return HttpNotFound();
            }
            return View(booking);
        }

        // POST: Bookings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Booking booking = db.Bookings.Find(id);
            db.Bookings.Remove(booking);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
